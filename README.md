# Weight
Last update: 2021

## Description
Scale for Weighing

## Hardware
* STM32F411RE
* HX711

### 3D Print
...  

## Software
FreeRTOS 10.4.3
C   11
C++ 14

## Usage
...  

## License
Creative Commons Attribution-NonCommercial-ShareAlike 4.0

