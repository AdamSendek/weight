/*
 * freertos.h
 *
 *  Created on: 18.04.2018
 *      Author: bodek
 */

#ifndef FREERTOS_MODULES_H_
#define FREERTOS_MODULES_H_

#include "FreeRTOS.h"
#include "task.h"

#ifdef __cplusplus
	extern "C" {
#endif

void configureTimerForRunTimeStats(void);
unsigned long getRunTimeCounterValue(void);
void vApplicationStackOverflowHook(TaskHandle_t xTask, char *pcTaskName);

#ifdef __cplusplus
	}
#endif

#endif /* FREERTOS_MODULES_H_ */
