#ifndef GPIO_H_
#define GPIO_H_

#include "main.h"

#ifdef __cplusplus
extern "C" {
#endif

void MX_GPIO_Init(void);

#ifdef __cplusplus
}
#endif

#endif
