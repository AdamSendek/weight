#include <i2c_driver/i2c_driver.h>


I2C_Driver::I2C_Driver(I2C_TypeDef *hendler)
{
	_mutex = xSemaphoreCreateMutex();
	if( _mutex == NULL ) while(1);

	_i2c_regs=hendler;
}


I2C_Driver::~I2C_Driver (void){
	vSemaphoreDelete(_mutex);
}


void I2C_Driver::_start(void){
	LL_I2C_AcknowledgeNextData(_i2c_regs, LL_I2C_ACK);
	LL_I2C_GenerateStartCondition(_i2c_regs); //START
	_local_timestamp=getTime();
	while(!LL_I2C_IsActiveFlag_SB(_i2c_regs)){
		if((getTime()-_local_timestamp)>=TIMEOUT_FLAG){
			_error_flag |= ERROR_SB;
			return;
		}
	}
}


void I2C_Driver::_send_address(){
	LL_I2C_TransmitData8(_i2c_regs, (_slave_address<<1) | I2C_REQUEST_WRITE); //CONTROL BYTE (ADDRESS + WRITE)
	_local_timestamp=getTime();
	while(!LL_I2C_IsActiveFlag_ADDR(_i2c_regs)){
		if((getTime()-_local_timestamp)>=TIMEOUT_FLAG){
			_error_flag |= ERROR_ADDR;
			return;
		}
	}
	LL_I2C_ClearFlag_ADDR(_i2c_regs);
}

void I2C_Driver::_send(uint8_t data){

	_local_timestamp=getTime();
	while(!LL_I2C_IsActiveFlag_TXE(_i2c_regs)){
		if((getTime()-_local_timestamp)>=TIMEOUT_FLAG){
			_error_flag |= ERROR_TXE;
			return;
		}
	}
	LL_I2C_TransmitData8(_i2c_regs, data);
//	while(!LL_I2C_IsActiveFlag_BTF(Hendler));
}

void I2C_Driver::_stop(void){
	_local_timestamp=getTime();
	while(!LL_I2C_IsActiveFlag_BTF(_i2c_regs)){
		if((getTime()-_local_timestamp)>=TIMEOUT_FLAG){
			_error_flag |= ERROR_BTF;
			return;
		}
	}
	LL_I2C_GenerateStopCondition(_i2c_regs);
}


void I2C_Driver::_reciver(uint8_t *buffer, uint8_t size){
	LL_I2C_TransmitData8(_i2c_regs, (_slave_address<<1) | I2C_REQUEST_READ); //CONTROL BYTE (ADDRESS + WRITE)
	_local_timestamp=getTime();
	while(!LL_I2C_IsActiveFlag_ADDR(_i2c_regs)){
		if((getTime()-_local_timestamp)>=TIMEOUT_FLAG){
			_error_flag |= ERROR_ADDR;
			return;
		}
	}
	LL_I2C_ClearFlag_ADDR(_i2c_regs);
	_local_timestamp=getTime();
	while(!LL_I2C_IsActiveFlag_RXNE(_i2c_regs)){
		if((getTime()-_local_timestamp)>=TIMEOUT_FLAG){
			_error_flag |= ERROR_RXNE;
			return;
		}
	}
	if(size<=2){
		LL_I2C_GenerateStopCondition(_i2c_regs); //STOP
		buffer[0] = LL_I2C_ReceiveData8(_i2c_regs);
		if(size<=1){
			LL_I2C_ReceiveData8(_i2c_regs);
		}else{
			buffer[1] = LL_I2C_ReceiveData8(_i2c_regs);
		}
	}else{
		uint8_t sizeEnd=size;
		while (sizeEnd>2){
			_local_timestamp=getTime();
			while(!LL_I2C_IsActiveFlag_RXNE(_i2c_regs)){
				if((getTime()-_local_timestamp)>=TIMEOUT_FLAG){
					_error_flag |= ERROR_RXNE;
					return;
				}
			}
				buffer[size-sizeEnd] = LL_I2C_ReceiveData8(_i2c_regs);
				LL_I2C_AcknowledgeNextData(_i2c_regs, LL_I2C_ACK);
				sizeEnd--;
		}
		_local_timestamp=getTime();
		while(!LL_I2C_IsActiveFlag_RXNE(_i2c_regs)){
			if((getTime()-_local_timestamp)>=TIMEOUT_FLAG){
				_error_flag |= ERROR_RXNE;
				return;
			}
		}
		LL_I2C_AcknowledgeNextData(_i2c_regs, LL_I2C_NACK);
		LL_I2C_GenerateStopCondition(_i2c_regs);
		sizeEnd--;
		buffer[size-sizeEnd] = LL_I2C_ReceiveData8(_i2c_regs);
		sizeEnd--;
		buffer[size-sizeEnd] = LL_I2C_ReceiveData8(_i2c_regs);
	}
}

int I2C_Driver::write(uint8_t address, uint8_t data){
	if(xSemaphoreTake(_mutex, TIMEOUT)){
	_slave_address=address;
	_start();
	_send_address();
	_send(data);
	_stop();
	xSemaphoreGive(_mutex);
}else{
		return -1;
	}
	return 0;
}

int I2C_Driver::write(uint8_t address, uint8_t reg, uint8_t data){
	if(xSemaphoreTake(_mutex, TIMEOUT)){
	_slave_address=address;
	_start();
	_send_address();
	_send(reg);
	_send(data);
	_stop();
	xSemaphoreGive(_mutex);
}else{
		return -1;
	}
	return 0;
}

int I2C_Driver::write(uint8_t address, uint8_t reg, uint8_t *data, uint8_t size){
	if(xSemaphoreTake(_mutex, TIMEOUT)){
	_slave_address=address;
	_start();
	_send_address();
	_send(reg);
	uint8_t i=0;
	for(;i<size;i++){
		_send(data[i]);
	}
	_stop();
	xSemaphoreGive(_mutex);
}else{
		return -1;
	}
	return 0;
}

int I2C_Driver::write(uint8_t address, uint8_t *data, uint8_t size){
	if(xSemaphoreTake(_mutex, TIMEOUT)){
	_slave_address=address;
	_start();
	_send_address();
	uint8_t i=0;
	for(;i<size;i++){
		_send(data[i]);
	}
	_stop();
	xSemaphoreGive(_mutex);
}else{
		return -1;
	}
	if(_error_flag>0){
		_error_flag=0;
		return -1;
	}
	return 0;
}

int I2C_Driver::read(uint8_t address, uint8_t *buffer, uint8_t size){
	if(xSemaphoreTake(_mutex, TIMEOUT)){
	_slave_address=address;
	_start();
	_reciver(buffer,size);//_reciver(buffer,1);
	xSemaphoreGive(_mutex);
}else{
		return -1;
	}
	if(_error_flag>0){
		_error_flag=0;
		return -1;
	}
	return 0;
}

int I2C_Driver::read(uint8_t address, uint8_t reg, uint8_t *buffer){
	if(xSemaphoreTake(_mutex, TIMEOUT)){
	_slave_address=address;
	_start();
	_send_address();
	_send(reg);
	_start();
	_reciver(buffer,1);
	xSemaphoreGive(_mutex);
}else{
		return -1;
	}
	if(_error_flag>0){
		_error_flag=0;
		return -1;
	}
	return 0;
}

int I2C_Driver::read(uint8_t address, uint8_t reg, uint8_t *buffer, uint8_t size){
	if(xSemaphoreTake(_mutex, TIMEOUT)){//odpytywanie	//96=1us
	_slave_address=address;
	_start();
	_send_address();
	_send(reg);
	_start();
	_reciver(buffer,size);
	xSemaphoreGive(_mutex);
	}else{
		return -1;
	}
	if(_error_flag>0){
		_error_flag=0;
		return -1;
	}
	return 0;
}
