#ifndef i2c_Driver_LL_
#define i2c_Driver_LL_


#include "main.h"
#include "FreeRTOS.h"
#include "semphr.h"
//#include "hr_timer/hr_timer.h"


#define TIMEOUT	0

#define I2C_REQUEST_WRITE                       0x00
#define I2C_REQUEST_READ                        0x01
//#pragma once

#define W true
#define R false

#define TIMEOUT_FLAG 	500	//ms
#define ERROR_SB		0b00000001
#define ERROR_ADDR		0b00000010
#define ERROR_TXE		0b00000100
#define ERROR_RXNE		0b00001000
#define ERROR_BTF		0b00010000




class I2C_Driver{

private:
	uint8_t _slave_address;
	SemaphoreHandle_t _mutex = NULL;
	I2C_TypeDef *_i2c_regs;


	uint64_t _local_timestamp;
	uint8_t _error_flag{0};

	void _start(void);
	void _send_address();
	void _send(uint8_t data);
	void _reciver(uint8_t *buffer, uint8_t size);
	void _stop(void);


public:
	I2C_Driver(I2C_TypeDef *hendler);
	~I2C_Driver(void);


	int write (uint8_t address, uint8_t data);	//zapis 1 Byte
	int write (uint8_t address, uint8_t reg, uint8_t data);	//zapis 1 Byte do rejestru
	int write (uint8_t address, uint8_t reg, uint8_t *data, uint8_t size);//zapis size Byte do rejestru
	int write (uint8_t address, uint8_t *data, uint8_t size);//zapis size Byte


	int read (uint8_t address, uint8_t *buffer, uint8_t size);	//odczyt size Byte
	int read (uint8_t address, uint8_t reg, uint8_t *buffer);	//odczyt 1 Byte z rejestru	//możliwe że jest teraz zbędne
	int read (uint8_t address, uint8_t reg, uint8_t *buffer, uint8_t size);	//odczyt size Byte z rejestru

};
#endif
