#include <hx711/hx711.h>
#include "stm32f411xe.h"

static hx711 *tensometr;

hx711::hx711(TIM_TypeDef *tim, uint32_t channel, GPIO_TypeDef *gpio, uint32_t pin, uint8_t mode){//LL_TIM_CHANNEL_CH1
	tensometr=this;
	_semaphore = xSemaphoreCreateBinary();
		if( _semaphore == NULL ) while(1);
		xSemaphoreGive(_semaphore);
		NVIC_EnableIRQ(EXTI0_IRQn);

	_tim = tim;
	_not_ready=1;
	LL_TIM_DisableCounter(_tim);
	_channel=channel;

	ccr(0);	//Channel = 0;
	_gpio=gpio;
	_pin=pin;
	_mode=mode;
	_measurement=0;
	_read_buffer=0;

	LL_EXTI_EnableIT_0_31(LL_EXTI_LINE_0);
	LL_EXTI_EnableEvent_0_31(LL_EXTI_LINE_0);
	LL_EXTI_EnableFallingTrig_0_31(LL_EXTI_LINE_0);
}

hx711::~hx711(void){
	vSemaphoreDelete(_semaphore);
}

void hx711::ccr(bool on_off){
	switch(_channel){
	case LL_TIM_CHANNEL_CH1:
		C1=on_off;
		break;
	case LL_TIM_CHANNEL_CH2:
		C2=on_off;
		break;
	case LL_TIM_CHANNEL_CH3:
		C3=on_off;
		break;
	case LL_TIM_CHANNEL_CH4:
		C4=on_off;
		break;
	default:
		//error
		break;
	}
}

void hx711::set_calibration(float offset, float scale){
	_offset = offset;
	_scale = scale;
}

void hx711::raw_read(void){
	_iteration++;
	if(_iteration<_mode){
		_read_buffer <<= 1;
		_read_buffer += (LL_GPIO_ReadInputPort(_gpio) & _pin);
	}else{
		ccr(0);	//Channel = 0;
		LL_TIM_DisableCounter(_tim);
		if( _htSubscribe != nullptr ){
			BaseType_t xHigherPriorityTaskWoken;
			xHigherPriorityTaskWoken = pdFALSE;
			xTaskNotifyIndexedFromISR(_htSubscribe, INDEX_NOTIFY_HX, NUMBER_NOTIFY_HX, eSetBits, &xHigherPriorityTaskWoken);
		}
	}
}

int hx711::read(TaskHandle_t subscribe){
	_htSubscribe=subscribe;
	if( _htSubscribe != nullptr ){
		return ERROR_NOTIFY;
	}
	uint32_t ulNotifiedValue;
	xTaskNotifyWaitIndexed(INDEX_NOTIFY_HX, 0, NUMBER_NOTIFY_HX_DATA_READY, &ulNotifiedValue, pdMS_TO_TICKS(TIMEOUT_READ));
		if( ( ulNotifiedValue & NUMBER_NOTIFY_HX_DATA_READY ) != 0 ) {

		NVIC_DisableIRQ(EXTI0_IRQn);
		if((_pin==0) || (_mode==0)){
			NVIC_EnableIRQ(EXTI0_IRQn);
//			"HX711: read invalid values parameaters function!"
			error_counter++;
			_htSubscribe = nullptr;
			return ERROR_INIT;
		}else{
			_read_buffer=0;
			_iteration=0;


			LL_TIM_EnableCounter(_tim);
			ccr(1);	//Channel = 1;


			xTaskNotifyWaitIndexed(INDEX_NOTIFY_HX, 0, NUMBER_NOTIFY_HX, &ulNotifiedValue, pdMS_TO_TICKS(TIMEOUT_READ));
			if( ( ulNotifiedValue & NUMBER_NOTIFY_HX ) != 0 ) {

				_read_buffer = _read_buffer>>(_mode - (SIZE_FRAME_DATA+1));//+1  dziwne ale ważne

				_measurement=(_read_buffer^RANGE_MIN);
			}else{
				LL_TIM_DisableCounter(_tim);
				ccr(0);	//Channel = 0;
				NVIC_EnableIRQ(EXTI0_IRQn);
//				"HX711: read difrente notification!"
				error_counter++;
				_htSubscribe = nullptr;
				return ERROR_READ;
			}
		}
	}else{
//		"HX711: read access hx711 timeout!"
		error_counter++;
		_htSubscribe = nullptr;
		return ERROR_MUTEX;
	}
	NVIC_EnableIRQ(EXTI0_IRQn);
	_htSubscribe = nullptr;
	return SUCCESS;
}

uint32_t hx711::get_measurement(TaskHandle_t subscribe){
	return _measurement;
}

float hx711::get_weight_kg(TaskHandle_t subscribe){//niemoże być ujemne
	int err=0;
	err=read(subscribe);
	if(err!=SUCCESS){
		return err;
	}
	return (float)(_measurement-_offset)*_scale;
}


void hx711::hx_ready(void){
	if( _htSubscribe != nullptr ){
		BaseType_t xHigherPriorityTaskWoken;
		xHigherPriorityTaskWoken = pdFALSE;
		xTaskNotifyIndexedFromISR(_htSubscribe, INDEX_NOTIFY_HX, NUMBER_NOTIFY_HX_DATA_READY, eSetBits, &xHigherPriorityTaskWoken);
	}
}


void hx711_sck_callback(void){
	tensometr->raw_read();
}

void hx711_dout_callback(void){
	tensometr->hx_ready();
}

