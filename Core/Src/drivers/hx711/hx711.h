#ifndef hx711_H_
#define hx711_H_

#include "main.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#define RANGE_MIN 0x800000

#define KG_TO_N		9.80665

//ilość odczytanych impulsów
#define ENHANCEMENT_128	25	//port A
#define ENHANCEMENT_32	26	//port B
#define ENHANCEMENT_64	27	//port A

#define SIZE_FRAME_DATA		24

//#define Channel	_tim->CCR2	//zmienić na małe litery //const referencja uint32_t

#define C1	_tim->CCR1
#define C2	_tim->CCR2
#define C3	_tim->CCR3
#define C4	_tim->CCR4

#define MAX_PULSE 27

#define INDEX_NOTIFY_HX 				1
#define NUMBER_NOTIFY_HX 				0x01
#define NUMBER_NOTIFY_HX_DATA_READY 	0x02
#define TIMEOUT_READ	100
#define TIMEOUT_MUTEX	100


#define INDEX_NOTIFY_HX_MEASUREMENT			1
#define NUMBER_NOTIFY_HX_MEASUREMENT_END	0x01

#define ERROR_NOTIFY	-4
#define ERROR_INIT		-2
#define ERROR_MUTEX		-1
#define ERROR_READ		-3
#define SUCCESS			 0


#define ERROR_NUMBER_RESTART	10	//po ilu błędach ma nastąpić restart licznika




class hx711{
	private:
	//TaskHandle_t	_htread{nullptr};
	TaskHandle_t	_htSubscribe{nullptr};


	GPIO_TypeDef *_gpio_null;


	SemaphoreHandle_t _semaphore = NULL;
	TIM_TypeDef *_tim;
	uint32_t _channel;
	GPIO_TypeDef *_gpio;
	uint32_t _pin;
	uint8_t _mode;

	bool _not_ready;
	uint8_t _size;
	uint8_t _lp;
	uint32_t _read_buffer;
	volatile uint8_t _iteration;
	float _offset;
	float _scale;
	uint32_t _measurement;
	uint8_t error_counter{0};

	void ccr(bool on_off);

	public:
	void raw_read(void);
	hx711(TIM_TypeDef *tim, uint32_t channel, GPIO_TypeDef *gpio, uint32_t pin, uint8_t mode);

	~hx711(void);
	int read(TaskHandle_t subscribe);
	void set_calibration(float offset, float scale);
	void hx_ready(void);
	uint32_t get_measurement(TaskHandle_t subscribe);
	float get_weight_kg(TaskHandle_t subscribe);

	void *thread(void *arg);

};

void hx711_sck_callback(void);
void hx711_dout_callback(void);

//extern hx711 *tensometr;



#endif
