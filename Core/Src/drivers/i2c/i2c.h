#ifndef I2C_H_
#define I2C_H_

#include "main.h"

#ifdef __cplusplus
extern "C" {
#endif

void MX_I2C1_Init(void);

#ifdef __cplusplus
}
#endif

#endif
