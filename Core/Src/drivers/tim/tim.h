#ifndef TIM_H_
#define TIM_H_

#include "main.h"

#ifdef __cplusplus
extern "C" {
#endif

void MX_TIM3_Init(void);

#ifdef __cplusplus
}
#endif

#endif
