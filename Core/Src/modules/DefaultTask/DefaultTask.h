#ifndef DEFAULT_TASK_H_
#define DEFAULT_TASK_H_

	#include "main.h"
	#include <i2c_driver/i2c_driver.h>
	#include "hx711/hx711.h"
	//#include "FreeRTOS.h"
	//#include "task.h"

	#define DEFAULT_TASK_STACKSIZE		(8*configMINIMAL_STACK_SIZE)
	#define DEFAULT_TASK_PRIORITY		6

	class DefaultTask{
	private:
		TaskHandle_t	_htread{nullptr};
	public:
		DefaultTask(void);
		~DefaultTask(void);
		void *thread(void *arg);
	};

#endif

