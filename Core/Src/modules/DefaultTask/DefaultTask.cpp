#include <DefaultTask/DefaultTask.h>
#include "task.h"
#include <new>

TaskFunction_t default_task(void* param);

DefaultTask::DefaultTask(void){
	if(xTaskCreate((TaskFunction_t)default_task, "default", DEFAULT_TASK_STACKSIZE, (void *)this, DEFAULT_TASK_PRIORITY, &this->_htread)!=pdPASS)
			::Error_Handler();
}

DefaultTask::~DefaultTask(void){
	vTaskDelete(this->_htread);
}

void * DefaultTask::thread(void *arg) {
	I2C_Driver *i2c1{nullptr};
	hx711	*weight{nullptr};

	i2c1 = (I2C_Driver *) pvPortMalloc(sizeof(I2C_Driver));	new(i2c1) I2C_Driver(I2C1);
	weight = (hx711 *) pvPortMalloc(sizeof(hx711));	new(weight) hx711(TIM3, LL_TIM_CHANNEL_CH2, GPIOB, LL_GPIO_PIN_0, ENHANCEMENT_128);

	uint8_t data_buf_size = 20;
	uint8_t data_buf[20];
	uint8_t address = 0x22;




	while(1){

		i2c1->write(address, (uint8_t *)data_buf, data_buf_size);
		weight->get_weight_kg(_htread);
		LL_mDelay(1000);

	}
}

TaskFunction_t default_task(void* param) {
	static_cast<DefaultTask *>(param)->thread(0);
	return 0;
}
