/**
 ******************************************************************************
 * File Name          : freertos.c
 * Description        : Code for freertos applications
 ******************************************************************************
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include <cstdio>
#include "main.h"
#include "freertos_modules.h"
#include "stm32f4xx_ll_usart.h"
#include "stm32f4xx_hal_def.h"

#ifdef __cplusplus
	extern "C" {
#endif

unsigned int uiAktualTask=0;

void vApplicationStackOverflowHook(TaskHandle_t xTask, char *pcTaskName) {
	/* Run time stack overflow checking is performed if
	 configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2. This hook function is
	 called if a stack overflow is detected. */
	UNUSED(xTask);
	//UNUSED(pcTaskName);
	char buff[128];
	volatile uint32_t len;
	len=sprintf(buff,"Stack overflow in %s\n",pcTaskName);
	UNUSED(len);

	while(1) {
/*#if defined SKYNAV_V1_0
        LED5_TOGGLE();
//#elif defined SKYNAV_V2_0
#else
        LED_TOGGLE();
#endif
*/	}
}

//void vApplicationIdleHook(void) {
//	LED5_TOGGLE();
//}

#ifdef __cplusplus
	}
#endif
